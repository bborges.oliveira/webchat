using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebChat.Models;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebChat.Controllers
{
    public class AccountController : Controller
    {
        // POST: Account
        public ActionResult Login(USERS login, string returnURL)
        {            
            //Check if there's a typed email before login
            if(ModelState.IsValid && login.EMAIL != null)
            {
                using (WebChatDBEntities db = new WebChatDBEntities())
                {
                    //Getting typed user in database
                    var vLogin = db.USERS.Where(p => p.EMAIL.Equals(login.EMAIL)).FirstOrDefault();

                    /* 
                     * Check if vLogin is empty. It can happens if the users doesn't exists.
                     * If it doesn't exists, it'll enter in the else condition
                     */
                    if(vLogin != null)
                    {
                        /* 
                         * Check if the returned user is active. If it doesn't, it'll enter in the else condition.
                         */
                        if(Equals(vLogin.ACTIVE, true))
                        {
                            /* 
                             * Check if the typed password is equal to that one in database. If it's not,
                             * it'll enter in the else condition.
                             */
                            if(Equals(vLogin.PASSWORD, login.PASSWORD))
                            {
                                //Store cookie with authentication data. The false parameter is to force the 
                                //user to logout when the browser is closed
                                FormsAuthentication.SetAuthCookie(vLogin.EMAIL, false);

                                //Validate the URL to be more secure
                                if(Url.IsLocalUrl(returnURL) && returnURL.Length > 1 && returnURL.StartsWith("/")
                                    && !returnURL.StartsWith("//") && returnURL.StartsWith("/\\"))
                                {
                                    return Redirect(returnURL);
                                }

                                //Create section to store the first_name
                                Session["First_Name"] = vLogin.FIRST_NAME;

                                //Create section to store the last_name
                                Session["Last_Name"] = vLogin.LAST_NAME;

                                //Create section to store the email (username)
                                Session["Username"] = vLogin.EMAIL;

                                //Reseting data from database
                                vLogin.CONNECTION_ID = "";
                                vLogin.IS_CHATING = false;
                                db.SaveChanges();

                                //Return to Home
                                return RedirectToAction("Index", "Home");
                            }
                            //Responsible for password validation
                            else
                            {
                                //Show the error message on the screen
                                ModelState.AddModelError("", "Wrong password!!!");

                                //Back to the login view
                                return View(new USERS());
                            }
                        }
                        //Responsible for checking if the user is active
                        else
                        {
                            //Show the error message on the screen
                            ModelState.AddModelError("", "User without access to the system!!!");

                            //Back to the login view
                            return View(new USERS());
                        }
                    }
                    //Responsible for checking if the user exists
                    else
                    {       
                       
                        //Show the error message on the screen
                        ModelState.AddModelError("", "Invalid e-mail!!!");

                        //Back to the login view
                        return View(new USERS());
                    }
                }
            }

            /*
             * If the fields aren't agree with the request, it returns to the login screen 
             * with the messages of the fields
             */
            return View(login);
        }

        /// <summary>
        /// Logout to the server
        /// </summary>
        public void Logout()
        {
            //Clear Session parameters and return to Login page
            Session.Abandon();
            Session.Clear();
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            Response.Redirect("/Account/Login", false);
        }
    }
}