# WebChat

Packages used: 
    1. bootstrap
    2. SignalR
    3. EntityFramework

Languages:
    1. C#
    2. ASP.NET
    3. JavaScript
    4. HTML

The first view is for login and register new users.
After logged in, the user can choose to logout or be online on the chat.
After being online, the user should insert the username (that is the registered e-mail) of other user to start chating.
Either if the other user doesn't exists, or isn't online or is already chating, the server creates an alert to inform the error.
If the user is free, then they both can start chating, and there's a button to finish the chat.