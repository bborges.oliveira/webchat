using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebChat.Models;

namespace WebChat.Controllers
{
    public class UsersController : Controller
    {
        private WebChatDBEntities db = new WebChatDBEntities();

        // GET: Users
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View(db.USERS.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USERS uSERS = db.USERS.Find(id);
            if (uSERS == null)
            {
                return HttpNotFound();
            }
            return View(uSERS);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            USERS newUser = new USERS();
            return View(newUser);
        }

        // POST: Users/Create
        // Para proteger-se contra ataques de excesso de postagem, ative as propriedades específicas às quais deseja se associar. 
        // Para obter mais detalhes, confira https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_LOGIN,EMAIL,PASSWORD,ACTIVE,PROFILE,FIRST_NAME,LAST_NAME,CONNECTION_ID")] USERS uSERS)
        {                

            if (ModelState.IsValid)
            {
                //Check if the E-mail already exists
                foreach(USERS newUser in db.USERS)
                {
                    if(newUser.EMAIL == uSERS.EMAIL)
                    {
                        //Show the error message on the screen
                        ModelState.AddModelError("", "E-mail account already registered!!!");
                        return View(uSERS);
                    }
                }

                //Setting values to the new User
                uSERS.ACTIVE = true;
                uSERS.PROFILE = "User";
                uSERS.CONNECTION_ID = "";
                uSERS.IS_CHATING = false;

                //Adding new User to the database
                db.USERS.Add(uSERS);

                //Updating database
                db.SaveChanges();

                //Return to the first view
                return RedirectToAction("Index");
            }

            return View(uSERS);
        }

        // GET: Users/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USERS uSERS = db.USERS.Find(id);
            if (uSERS == null)
            {
                return HttpNotFound();
            }
            return View(uSERS);
        }

        // POST: Users/Edit/5
        // Para proteger-se contra ataques de excesso de postagem, ative as propriedades específicas às quais deseja se associar. 
        // Para obter mais detalhes, confira https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_LOGIN,EMAIL,PASSWORD,ACTIVE,PROFILE,FIRST_NAME,LAST_NAME,CONNECTION_ID")] USERS uSERS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uSERS).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
            }
            return View(uSERS);
        }

        // GET: Users/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USERS uSERS = db.USERS.Find(id);
            if (uSERS == null)
            {
                return HttpNotFound();
            }
            return View(uSERS);
        }

        // POST: Users/Delete/5
        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            USERS uSERS = db.USERS.Find(id);
            db.USERS.Remove(uSERS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Check if the user exists
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public bool CheckUser(string username)
        {
            //Looking for the user in the database
            foreach(USERS newUser in db.USERS)
            {
                if(newUser.EMAIL == username)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Store connection ID of current user
        /// </summary>
        /// <param name="connId"></param>
        [Authorize]
        [HttpPost]        
        public void StoreConnectionID(string connId)
        {
            int userId = -1;

            if (ModelState.IsValid)
            {
                //Looking for the User's data in database using his username
                foreach (USERS newUser in db.USERS)
                {
                    if (Session["Username"].ToString() == newUser.EMAIL)
                    {
                        userId = newUser.ID_LOGIN;                        
                        break;
                    }
                }

                //If the user exists, update its CONNECTION_ID
                if(userId != -1)
                {
                    USERS updateUser = db.USERS.Find(userId);
                    updateUser.CONNECTION_ID = connId;
                    db.SaveChanges();
                }                
            }
        }

        /// <summary>
        /// Clear stored connection ID
        /// </summary>
        /// <param name="username"></param>
        [Authorize]
        [HttpPost]
        public void ClearConnectionID(string username)
        {
            if(ModelState.IsValid)
            {
                var getUser = db.USERS.Where(p => p.EMAIL.Equals(username)).FirstOrDefault();
                if(getUser != null)
                {
                    //If the user exists in database, clear its connection id and update database
                    getUser.CONNECTION_ID = "";
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get connection id of desired user
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public string GetConnectionID(string username)
        {
            if(ModelState.IsValid)
            {
                var getUser = db.USERS.Where(p => p.EMAIL.Equals(username)).FirstOrDefault();
                if(getUser != null)
                {
                    //If the user exists in database, return its connection id
                    return getUser.CONNECTION_ID;
                }                
            }

            return "";
        }

        /// <summary>
        /// Set database field indicating that the user is chating
        /// </summary>
        /// <param name="username"></param>
        [Authorize]
        [HttpPost]
        public void StartChating(string username)
        {
            if(ModelState.IsValid)
            {
                var getUser = db.USERS.Where(p => p.EMAIL.Equals(username)).FirstOrDefault();
                if(getUser != null)
                {
                    //If the user exists in database, set the IS_CHATING parameter to indicate the user is chating
                    getUser.IS_CHATING = true;
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Set database field indicating that the user stops chating
        /// </summary>
        /// <param name="username"></param>
        [Authorize]
        [HttpPost]
        public void StopChating(string username)
        {
            if(ModelState.IsValid)
            {
                var getUser = db.USERS.Where(p => p.EMAIL.Equals(username)).FirstOrDefault();
                if(getUser != null)
                {
                    //If the user exists in database, set the IS_CHATING parameter to false indicating the user stops chating
                    getUser.IS_CHATING = false;
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Check if the user is chating
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public bool IsChating(string username)
        {
            if(ModelState.IsValid)
            {
                var getUser = db.USERS.Where(p => p.EMAIL.Equals(username)).FirstOrDefault();
                if(getUser != null)
                {
                    //If the user exists, return the chat status
                    if ((bool)getUser.IS_CHATING) return true;
                    else return false;
                }
            }

            return false;
        }
    }
}
