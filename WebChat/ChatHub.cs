using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebChat
{
    public class ChatHub : Hub
    {
        public void Send(string id, string name, string message)
        {
            // Call the addNewMessageToPage method to update clients.
            Clients.Client(Context.ConnectionId).addNewMessageToPage(name, message);
            Clients.Client(id).addNewMessageToPage(name, message);
            
        }
    }
}